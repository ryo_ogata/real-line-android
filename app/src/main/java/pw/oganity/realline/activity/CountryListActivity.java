package pw.oganity.realline.activity;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.MenuItem;

import pw.oganity.realline.App;
import pw.oganity.realline.R;
import pw.oganity.realline.fragment.CountryListFragment;
import pw.oganity.realline.fragment.SearchWordTimeLineFragment;

public class CountryListActivity extends FragmentActivity {

    private final static String TAG = "CountryListActivity";

    // Intent Keys
    public static final String SEARCH_WORD = "searchWord";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String word = null;
        Intent intent = getIntent();
        if (intent != null) {
            word = intent.getStringExtra(SEARCH_WORD);
        }
        if (savedInstanceState == null) {
            pushFragment(createFragment(word));
        }

        setContentView(R.layout.activity_frame);
        ActionBar actionbar = getActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        App.getInstance().analytics().reportActivityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        App.getInstance().analytics().reportActivityStop(this);
    }

    private Fragment createFragment(String word) {
        if (word == null) {
            return CountryListFragment.newInstance();
        } else {
            return SearchWordTimeLineFragment.newInstance(word);
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            popBackFragment();
        } else {
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                popBackFragment();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void pushFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.slide_in, R.anim.slide_out, 0, R.anim.pop_out)
                .replace(R.id.container, fragment)
                .addToBackStack(null)
                .commit();
    }

    public void popBackFragment() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStackImmediate();
        } else {
            finish();
        }
    }

    public void startHomeActivity() {
        startActivity(new Intent(this, HomeActivity.class)
                .setFlags(App.FLAG_ACTIVITY_CLEAR_AND_NEW_TASK));
    }
}
