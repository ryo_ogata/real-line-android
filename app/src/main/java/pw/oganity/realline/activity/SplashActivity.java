package pw.oganity.realline.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import pw.oganity.realline.App;
import pw.oganity.realline.R;
import pw.oganity.realline.SessionRecorder;


public class SplashActivity extends Activity {

    private TwitterLoginButton mTwitterButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setUpViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (SessionRecorder.getTwitterSession() != null) {
                    startHomeActivity();
                } else {
                    showTwitterSignInButton();
                }
            }
        }, 1800);
    }

    @Override
    public void onStart() {
        super.onStart();
        App.getInstance().analytics().reportActivityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        App.getInstance().analytics().reportActivityStop(this);
    }

    private void showTwitterSignInButton() {
        if (mTwitterButton.getVisibility() == View.VISIBLE) {
            return;
        }
        mTwitterButton.setVisibility(View.VISIBLE);
        YoYo.with(Techniques.BounceInDown)
                .delay(0)
                .duration(1200)
                .withListener(new Animator.AnimatorListener() {

                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        bounceButton(0);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(mTwitterButton);
    }

    private void bounceButton(final int delay) {
        YoYo.with(Techniques.Bounce)
                .delay(delay)
                .duration(800)
                .withListener(new Animator.AnimatorListener() {

                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        bounceButton(400);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(mTwitterButton);
    }


    private void setUpViews() {
        setUpTwitterButton();
    }

    private void setUpTwitterButton() {
        mTwitterButton = (TwitterLoginButton) findViewById(R.id.twitter_button);
        mTwitterButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                Toast.makeText(getApplicationContext(),
                        getResources().getString(R.string.toast_twitter_signin_success),
                        Toast.LENGTH_SHORT).show();
                SessionRecorder.recordSessionActive("Login: twitter account active", result.data);
                startHomeActivity();
            }

            @Override
            public void failure(TwitterException exception) {
                Toast.makeText(getApplicationContext(),
                        getResources().getString(R.string.toast_twitter_signin_fail),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mTwitterButton.onActivityResult(requestCode, resultCode, data);
    }

    private void startHomeActivity() {
        startActivity(new Intent(this, HomeActivity.class)
                .setFlags(App.FLAG_ACTIVITY_CLEAR_AND_NEW_TASK));
    }

}
