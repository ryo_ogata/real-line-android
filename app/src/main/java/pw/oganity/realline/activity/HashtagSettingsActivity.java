package pw.oganity.realline.activity;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.MenuItem;

import pw.oganity.realline.App;
import pw.oganity.realline.R;
import pw.oganity.realline.fragment.HashtagListFragment;

public class HashtagSettingsActivity extends FragmentActivity {

    private final static String TAG = "CountryListActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String word = null;
        if (savedInstanceState == null) {
            pushFragment(createFragment(word));
        }

        setContentView(R.layout.activity_frame);
        ActionBar actionbar = getActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        App.getInstance().analytics().reportActivityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        App.getInstance().analytics().reportActivityStop(this);
    }

    private Fragment createFragment(String word) {
        return HashtagListFragment.newInstance();
    }

    @Override
    public void onBackPressed() {
        popBackFragment();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                popBackFragment();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void pushFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .replace(R.id.container, fragment)
                .addToBackStack(null)
                .commit();
    }

    public void popBackFragment() {
        startHomeActivity();
    }

    public void startHomeActivity() {
        startActivity(new Intent(this, HomeActivity.class)
                .setFlags(App.FLAG_ACTIVITY_CLEAR_AND_NEW_TASK));
    }
}
