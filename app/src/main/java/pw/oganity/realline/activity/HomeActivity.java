package pw.oganity.realline.activity;

import android.animation.LayoutTransition;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;
import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.models.User;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import pw.oganity.realline.App;
import pw.oganity.realline.R;
import pw.oganity.realline.SessionRecorder;
import pw.oganity.realline.api.RLTwitterApiClient;
import pw.oganity.realline.fragment.TimeLineFragment;
import pw.oganity.realline.realm.Bucket;
import pw.oganity.realline.realm.Hashtag;

public class HomeActivity extends FragmentActivity {

    private static final String TAG = "HomeActivity";
    private TimelinePagerAdapter mPagerAdapter;
    private DrawerLayout mDrawerLayout;
    private ViewGroup mDrawerRoot;
    private ImageView mProfileBackgroundImage;
    private ImageView mProfileImage;

    private ExpandableListView mExpandListView;
    private BucketArrayAdapter mDrawerArrayAdapter;
    private ViewPager mViewPager;
    private TextView mAuthorFullName;
    private TextView mAuthorScreenName;
    private View mActionAbout;
    private View mActionLogout;
    private TextView mNoBucketText;

    private List<Bucket> mBuckets;
    private Button mSettingsButton;
    private PagerTabStrip mPagerTabStrip;
    private View mSearchHashtag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        final android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        mPagerAdapter = new TimelinePagerAdapter(fm, getResources());
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mPagerAdapter);

        // Drawer
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerRoot = (ViewGroup) findViewById(R.id.left_drawer);
        mProfileBackgroundImage = (ImageView) mDrawerRoot.findViewById(R.id.profile_background_image);
        mProfileImage = (ImageView) mDrawerRoot.findViewById(R.id.profile_image);
        mAuthorFullName = (TextView) mDrawerRoot.findViewById(R.id.author_full_name);
        mAuthorScreenName = (TextView) mDrawerRoot.findViewById(R.id.author_screen_name);
        mExpandListView = (ExpandableListView) mDrawerRoot.findViewById(R.id.expand_list_view);
        mSearchHashtag = mDrawerRoot.findViewById(R.id.action_show_trends);
        mActionAbout = mDrawerRoot.findViewById(R.id.action_show_about);
        mActionLogout = mDrawerRoot.findViewById(R.id.action_logout);
        mNoBucketText = (TextView) mDrawerRoot.findViewById(R.id.no_buckets);
        mSettingsButton = (Button) mDrawerRoot.findViewById(R.id.settings_button);
    }

    @Override
    public void onStart() {
        super.onStart();
        App.getInstance().analytics().reportActivityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        App.getInstance().analytics().reportActivityStop(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Buckets
        mBuckets =
                Realm.getInstance(this).where(Bucket.class).findAllSorted("createdAt");
        mPagerAdapter.setBuckets(mBuckets);
        mPagerAdapter.notifyDataSetChanged();

        // ExpandableListview
        mDrawerArrayAdapter = new BucketArrayAdapter(this, getLayoutInflater(), mBuckets);
        mExpandListView.setAdapter(mDrawerArrayAdapter);

        mExpandListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long id) {
                int itemType = ExpandableListView.getPackedPositionType(id);

                if (itemType == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
                    int childPosition = ExpandableListView.getPackedPositionChild(id);
                    int groupPosition = ExpandableListView.getPackedPositionGroup(id);
                    Bucket bucket = mBuckets.get(groupPosition);
                    Hashtag hashtag = bucket.getHashtags().get(childPosition);
                    showWriteConfirmDialog(hashtag.getName());
                    return true;
                }
                return false;
            }
        });
        mExpandListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int j, long l) {

                startSearchWordTimelineFragment(
                        mBuckets.get(i).getHashtags().get(j).getName()
                );
                return false;
            }
        });
        mDrawerArrayAdapter.setOnGroupClickListener(new OnGroupClickListener() {
            @Override
            public void onClick(int i, boolean b) {
                mViewPager.setCurrentItem(i + 1, true);
            }
        });

        mDrawerArrayAdapter.notifyDataSetChanged();
        boolean hasBuckets = (mBuckets.size() > 0);
        mNoBucketText.setVisibility((!hasBuckets) ? View.VISIBLE : View.INVISIBLE);
        mSettingsButton.setVisibility((hasBuckets) ? View.VISIBLE : View.INVISIBLE);
        if (!hasBuckets) {
            bounceSearchHashtagButton(1000);
        }

        TwitterSession session = SessionRecorder.getTwitterSession();
        new RLTwitterApiClient(session).getUsersService()
                .usersShow(session.getUserName(), new Callback<User>() {
                    @Override
                    public void success(Result<User> result) {
                        Picasso.with(getApplicationContext())
                                .load(result.data.profileBackgroundImageUrl).into(mProfileBackgroundImage);
                        Picasso.with(getApplicationContext())
                                .load(result.data.profileImageUrl).into(mProfileImage);
                        mAuthorFullName.setText(result.data.name);
                        mAuthorScreenName.setText("@" + result.data.screenName);
                    }

                    @Override
                    public void failure(TwitterException e) {
                        throw e;
                    }
                });
        mSearchHashtag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startCountryListActivity();
            }
        });
        mActionAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAboutDialog();
            }
        });
        mActionLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLogOutConfirmDialog();
            }
        });
        mSettingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startHashtagSettingActivity();
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setListViewHeightBasedOnChildren(mExpandListView);
            }
        }, 1000);
    }

    private void bounceSearchHashtagButton(int delay) {
        YoYo.with(Techniques.Tada)
                .delay(delay)
                .duration(2000)
                .withListener(new Animator.AnimatorListener() {

                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        bounceSearchHashtagButton(400);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(mSearchHashtag);
    }

    private void showWriteConfirmDialog(final String word) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.write_confirm_title)
                .setMessage(getString(R.string.write_confirm_message, word))
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        TweetComposer.Builder composer = new TweetComposer.Builder(getApplicationContext())
                                .text(word + " ");
                        Intent intent = composer.createIntent();
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        getApplicationContext().startActivity(intent);
                    }
                })
                .setNegativeButton(android.R.string.cancel, null)
                .show();
    }

    private void startHashtagSettingActivity() {
        startActivity(new Intent(this, HashtagSettingsActivity.class));
    }

    private void showLogOutConfirmDialog() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.logout_confirm_title)
                .setMessage(R.string.logout_confirm_message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (i == AlertDialog.BUTTON_POSITIVE) {
                            appLogOut();
                        }
                    }
                })
                .setNegativeButton(android.R.string.cancel, null)
                .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setLayoutTransition(new LayoutTransition());
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                searchView.clearFocus();
                startSearchWordTimelineFragment(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                break;
            case R.id.action_word:
                startCountryListActivity();
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void appLogOut() {
        Twitter.getSessionManager().clearActiveSession();
        TwitterCore core = Fabric.getKit(TwitterCore.class);
        core.logOut();
        startSplashActivity();
    }

    private void startSplashActivity() {
        startActivity(new Intent(this, SplashActivity.class)
                .setFlags(App.FLAG_ACTIVITY_CLEAR_AND_NEW_TASK));
    }

    private void showAboutDialog() {
        View aboutView = getLayoutInflater().inflate(R.layout.about_view, null, false);
        String version = "N/A";
        try {
            version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (Exception e) {
        }
        ((TextView) aboutView.findViewById(R.id.about_version)).setText(version);

        aboutView.findViewById(R.id.link_twitter_text).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?screen_name=ryousuke_o"));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/ryousuke_o"));
                    startActivity(intent);
                }
            }
        });
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.about_dialog_title))
                .setView(aboutView)
                .setPositiveButton(android.R.string.ok, null)
                .show();
    }

    private void startCountryListActivity() {
        startActivity(new Intent(this, CountryListActivity.class));
    }

    private void startSearchWordTimelineFragment(String word) {
        startActivity(new Intent(this, CountryListActivity.class)
                .putExtra(CountryListActivity.SEARCH_WORD, word));
    }

    public static class TimelinePagerAdapter extends FragmentStatePagerAdapter {
        private Resources resources;

        private List<Bucket> mBuckets;

        public void setBuckets(List<Bucket> mBuckets) {
            this.mBuckets = mBuckets;
        }

        public TimelinePagerAdapter(android.support.v4.app.FragmentManager fm, Resources resources) {
            super(fm);
            this.resources = resources;
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            if (position == 0) {
                return TimeLineFragment.newInstance();
            }
            Bucket bucket = mBuckets.get(position - 1);
            List<String> hashtags = new ArrayList<>();
            for (Hashtag hashtag : bucket.getHashtags()) {
                hashtags.add(hashtag.getName());
            }
            return TimeLineFragment.newInstance(hashtags);
        }

        @Override
        public int getCount() {
            return (mBuckets == null) ? 1 : mBuckets.size() + 1;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position == 0) {
                return resources.getString(R.string.search_timeline_title);
            }
            if (mBuckets == null) {
                return null;
            }
            return mBuckets.get(position - 1).getName();
        }

    }

    interface OnGroupClickListener {
        void onClick(int i, boolean b);
    }

    class BucketArrayAdapter extends BaseExpandableListAdapter {
        private Context mContext;
        private LayoutInflater mInfrater;
        private List<Bucket> mBuckets;
        private OnGroupClickListener mOnGroupClickListener;

        public void setOnGroupClickListener(OnGroupClickListener onGroupClickListener) {
            this.mOnGroupClickListener = onGroupClickListener;
        }

        public BucketArrayAdapter(Context context, LayoutInflater inflater, List<Bucket> buckets) {
            super();
            this.mContext = context;
            this.mInfrater = inflater;
            this.mBuckets = buckets;
        }

        public void setBuckets(List<Bucket> buckets) {
            this.mBuckets = buckets;
        }

        @Override
        public int getGroupCount() {
            return mBuckets.size();
        }

        @Override
        public int getChildrenCount(int i) {
            return mBuckets.get(i).getHashtags().size();
        }

        @Override
        public Object getGroup(int i) {
            return mBuckets.get(i);
        }

        @Override
        public Object getChild(int i, int j) {
            return mBuckets.get(i).getHashtags().get(j);
        }

        @Override
        public long getGroupId(int i) {
            return i;
        }

        @Override
        public long getChildId(int i, int j) {
            return j;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getGroupView(final int i, final boolean b, View convertView, ViewGroup viewGroup) {
            if (convertView == null) {
                convertView = this.mInfrater.inflate(R.layout.expandable_list_item_buckets, null, false);
            }
            TextView textView = ((TextView) convertView.findViewById(R.id.bucket_name_text));
            textView.setText(mBuckets.get(i).getName());
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnGroupClickListener != null) {
                        mOnGroupClickListener.onClick(i, b);
                    }
                }
            });
            return convertView;
        }

        @Override
        public View getChildView(final int i, final int j, final boolean b, View convertView, ViewGroup viewGroup) {
            if (convertView == null) {
                convertView = this.mInfrater.inflate(R.layout.expandable_list_item_hashtag, null, false);
            }
            ((TextView) convertView.findViewById(R.id.hashtag_name_text))
                    .setText(mBuckets.get(i).getHashtags().get(j).getName());
            return convertView;
        }

        @Override
        public boolean isChildSelectable(int i, int j) {
            return true;
        }

        @Override
        public void onGroupCollapsed(int groupPosition) {
            super.onGroupCollapsed(groupPosition);
            setListViewHeightBasedOnChildren(mExpandListView);
        }

        @Override
        public void onGroupExpanded(int groupPosition) {
            super.onGroupExpanded(groupPosition);
            setListViewHeightBasedOnChildren(mExpandListView);
        }

    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
}
