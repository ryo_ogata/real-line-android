package pw.oganity.realline.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import pw.oganity.realline.R;
import pw.oganity.realline.vo.CountryListItemVo;

/**
 * Created by rogata on 2015/05/30.
 */
public class CountryListAdapter extends ArrayAdapter<CountryListItemVo> {

    private Picasso mImageLoader;
    private LayoutInflater mInflater;

    public CountryListAdapter(Context context, int resource) {
        super(context, resource);
        init();
    }

    private void init() {
        mImageLoader = Picasso.with(getContext());
        mInflater = ((Activity) getContext()).getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null || convertView.getTag() == null) {
            convertView = mInflater.inflate(R.layout.place_list_item_view, null, false);
            convertView.setTag(new ViewHolder(convertView));
        }
        CountryListItemVo model = getItem(position);
        final ViewHolder h = (ViewHolder) convertView.getTag();

        if (model.getCountryImageUrl() != null) {
            mImageLoader.load(model.getCountryImageUrl()).into(h.mCountryImageView);
        } else {
            h.mCountryImageView.setImageResource(R.drawable.ic_worldwide);
        }
        h.mCountryNameTextView.setText(model.getCountryName());
        return convertView;
    }

    class ViewHolder {
        ImageView mCountryImageView;
        TextView mCountryNameTextView;

        ViewHolder(View v) {
            mCountryImageView = (ImageView) v.findViewById(R.id.country_image_view);
            mCountryNameTextView = (TextView) v.findViewById(R.id.country_name_view);
        }
    }
}
