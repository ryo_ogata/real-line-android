package pw.oganity.realline.view;

import android.content.Context;
import android.util.AttributeSet;

import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import pw.oganity.realline.R;

/**
 * Created by r-ogata on 15/05/12.
 */
public class RLTwitterLoginButton extends TwitterLoginButton {
    public RLTwitterLoginButton(Context context) {
        super(context);
        init();
    }

    public RLTwitterLoginButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RLTwitterLoginButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        if (isInEditMode()) {
            return;
        }
//        setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable
//                .ic_signin_twitter), null, null, null);
        setBackgroundResource(R.drawable.sign_up_button);
//        setTextSize(20);
//        setPadding(30, 0, 10, 0);
//        setTextColor(getResources().getColor(R.color.tw__blue_default));
//        setTypeface(App.getInstance().getTypeface());
    }
}
