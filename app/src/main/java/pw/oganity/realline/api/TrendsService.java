package pw.oganity.realline.api;

import com.twitter.sdk.android.core.Callback;

import java.util.List;

import pw.oganity.realline.api.model.TrendsAvailableModel;
import pw.oganity.realline.api.model.TrendsPlaceModel;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by r-ogata on 15/05/21.
 */
public interface TrendsService {
    @GET("/1.1/trends/available.json")
    void trendsAvailable(Callback<List<TrendsAvailableModel>> cb);

    @GET("/1.1/trends/place.json")
    void trendsPlace(@Query("id") int id, Callback<List<TrendsPlaceModel>> cb);
}