package pw.oganity.realline.api;

import com.twitter.sdk.android.core.Session;
import com.twitter.sdk.android.core.TwitterApiClient;

/**
 * Created by r-ogata on 15/05/21.
 */
public class RLTwitterApiClient extends TwitterApiClient {
    public RLTwitterApiClient(Session session) {
        super(session);
    }

    public TrendsService getTrendsService() {
        return getService(TrendsService.class);
    }

    public UsersService getUsersService() {
        return getService(UsersService.class);
    }
}
