package pw.oganity.realline.api;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.models.User;

import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by r-ogata on 15/05/21.
 */
public interface UsersService {
    @GET("/1.1/users/show.json")
    void usersShow(@Query("screen_name") String screenName, Callback<User> cb);
}