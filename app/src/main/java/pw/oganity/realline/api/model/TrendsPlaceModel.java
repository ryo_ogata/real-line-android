package pw.oganity.realline.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by r-ogata on 15/05/21.
 */
@Data
@EqualsAndHashCode
public class TrendsPlaceModel {
    @SerializedName("locations.name")
    String locationName;
    @SerializedName("locations.woeid")
    String woeid;
    @SerializedName("trends")
    List<TrendsModel> trends;
}
