package pw.oganity.realline.api.model;

import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by r-ogata on 15/05/21.
 */
@Data
@EqualsAndHashCode
public class TrendsModel {
    @SerializedName("name")
    String hashtag;
    @SerializedName("query")
    String hashtagQuery;
}
