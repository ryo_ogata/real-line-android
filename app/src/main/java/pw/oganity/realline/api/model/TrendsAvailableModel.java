package pw.oganity.realline.api.model;

import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by r-ogata on 15/05/21.
 */
@Data
@EqualsAndHashCode
public class TrendsAvailableModel {
    String country;
    String countryCode;
    @SerializedName("name")
    String countryName;
    @SerializedName("parentid")
    int parentId;
    @SerializedName("placeType.code")
    int placeTypeCode;
    @SerializedName("placeType.name")
    int placeTypeName;
    int woeid;
}
