package pw.oganity.realline.realm;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;
import lombok.EqualsAndHashCode;

/**
 * Created by rogata on 2015/05/26.
 * <p/>
 * https://dev.twitter.com/rest/reference/get/trends/available
 */
@EqualsAndHashCode(callSuper = false)
public class Bucket extends RealmObject {
    @PrimaryKey
    private String uuid;
    @Index
    private String name;

    private RealmList<Hashtag> hashtags;

    private Date createdAt;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RealmList<Hashtag> getHashtags() {
        return hashtags;
    }

    public void setHashtags(RealmList<Hashtag> hashtags) {
        this.hashtags = hashtags;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

}
