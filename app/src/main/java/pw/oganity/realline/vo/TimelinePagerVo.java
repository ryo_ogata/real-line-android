package pw.oganity.realline.vo;

import lombok.Data;
import pw.oganity.realline.realm.Bucket;

/**
 * Created by rogata on 2015/06/01.
 */
@Data
public class TimelinePagerVo {
    Bucket bucket;

    public TimelinePagerVo() {
    }

    public TimelinePagerVo(Bucket bucket) {
        this.bucket = bucket;
    }
}
