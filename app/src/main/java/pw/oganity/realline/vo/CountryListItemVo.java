package pw.oganity.realline.vo;

import java.io.Serializable;
import java.util.Locale;

import lombok.Data;
import pw.oganity.realline.api.model.TrendsAvailableModel;

/**
 * Created by rogata on 2015/05/29.
 */
@Data
public class CountryListItemVo implements Serializable {
    Boolean isWorldWide;
    Boolean isMyCountry;
    String countryImageUrl;
    String countryCode;
    String countryName;
    int woeid;
    int parentId;

    public static CountryListItemVo newInstance(TrendsAvailableModel model) {
        CountryListItemVo vo = new CountryListItemVo();
        vo.woeid = model.getWoeid();
        vo.parentId = model.getParentId();
        String imageUrl = (model.getCountryCode() == null) ? null :
                String.format("http://www.geonames.org/flags/x/%s.gif", model.getCountryCode().toLowerCase());
        vo.countryImageUrl = imageUrl;
        vo.countryCode = model.getCountryCode();
        vo.isWorldWide = model.getWoeid() == 1;
        String myCountryCode = Locale.getDefault().getCountry().toUpperCase();
        if (model.getCountryCode() != null) {
            vo.isMyCountry = model.getCountryCode().equals(myCountryCode);
        } else {
            vo.isMyCountry = false;
        }
        vo.countryName = model.getCountryName();
        return vo;
    }
}
