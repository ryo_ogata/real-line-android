package pw.oganity.realline.vo;

import lombok.Data;
import pw.oganity.realline.realm.Bucket;
import pw.oganity.realline.realm.Hashtag;

/**
 * Created by rogata on 2015/06/01.
 */
@Data
public class DrawerItemVo {
    Bucket bucket;
    Hashtag hashtag;

    public DrawerItemVo(Bucket bucket) {
        this.bucket = bucket;
    }

    public DrawerItemVo(Hashtag hashtag) {
        this.hashtag = hashtag;
    }
}
