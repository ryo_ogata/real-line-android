/**
 * Copyright (C) 2014 Twitter Inc and other contributors.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pw.oganity.realline;

import android.app.Application;
import android.content.Intent;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mopub.common.MoPub;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;
import com.twitter.sdk.android.tweetui.TweetUi;

import io.fabric.sdk.android.DefaultLogger;
import io.fabric.sdk.android.Fabric;

/**
 * This class represents the Application and extends Application it is used to initiate the
 * application.
 */

public class App extends Application {

    public final static String CRASHLYTICS_KEY_SESSION_ACTIVATED = "session_activated";

    public final static int FLAG_ACTIVITY_CLEAR_AND_NEW_TASK = Intent.FLAG_ACTIVITY_NEW_TASK | android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME;

    private static App singleton;
    private TwitterAuthConfig authConfig;

    private GoogleAnalytics analytics;
    private Tracker tracker;

    public static App getInstance() {
        return singleton;
    }

    public GoogleAnalytics analytics() {
        return analytics;
    }

    public Tracker tracker() {
        return tracker;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;
        authConfig
                = new TwitterAuthConfig(BuildConfig.TWITTER_CONSUMER_KEY, BuildConfig.TWITTER_CONSUMER_SECRET);

        final Fabric fabric = new Fabric.Builder(this)
                .kits(new Crashlytics(), new Twitter(authConfig), new TweetUi(), new TweetComposer(), new MoPub())
                .logger(new DefaultLogger(Log.DEBUG))
                .debuggable(true)
                .build();

        Fabric.with(fabric);

        // to be next vesion...
        analytics = GoogleAnalytics.getInstance(this);
        analytics.setLocalDispatchPeriod(1800);

        tracker = analytics.newTracker("UA-58938833-3");
        tracker.enableExceptionReporting(true);
        tracker.enableAdvertisingIdCollection(true);
        tracker.enableAutoActivityTracking(true);
    }

    public void sendAddHashtag(String hashtag) {
        tracker.send(
                new HitBuilders.EventBuilder("hashtag", "add")
                        .setLabel(hashtag)
                        .build());
    }

    public void sendDeleteHashtag(String hashtag) {
        tracker.send(
                new HitBuilders.EventBuilder("hashtag", "delete")
                        .setLabel(hashtag)
                        .build());
    }

    public void sendCreateBucket(String bucketName) {
        tracker.send(
                new HitBuilders.EventBuilder("bucket", "add")
                        .setLabel(bucketName)
                        .build());
    }

    public void sendDeleteBucket(String bucketName) {
        tracker.send(
                new HitBuilders.EventBuilder("bucket", "delete")
                        .setLabel(bucketName)
                        .build());
    }

    public void sendRenameBucket(String bucketNameAfter) {
        tracker.send(
                new HitBuilders.EventBuilder("bucket", "rename")
                        .setLabel(bucketNameAfter)
                        .build());
    }

}
