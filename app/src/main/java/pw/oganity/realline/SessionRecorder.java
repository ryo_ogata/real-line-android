package pw.oganity.realline;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Session;
import com.twitter.sdk.android.core.TwitterSession;

public class SessionRecorder {
    public static TwitterSession getTwitterSession() {
        TwitterSession twitterSession = Twitter.getSessionManager().getActiveSession();
        if (twitterSession != null) {
            recordSessionActive("Splash: user with active Twitter session", twitterSession);
            return twitterSession;
        } else {
            recordSessionInactive("Splash: anonymous user");
            return null;
        }
    }

    public static void recordSessionActive(String message, Session session) {
        recordSessionActive(message, String.valueOf(session.getId()));
    }

    public static void recordSessionInactive(String message) {
        recordSessionState(message, null, false);
    }

    private static void recordSessionActive(String message, String userIdentifier) {
        recordSessionState(message, userIdentifier, true);
    }

    private static void recordSessionState(String message,
                                           String userIdentifier,
                                           boolean active) {
        CrashlyticsCore core = Crashlytics.getInstance().core;
        core.log(message);
        core.setUserIdentifier(userIdentifier);
        core.setBool(App.CRASHLYTICS_KEY_SESSION_ACTIVATED, active);
    }
}
