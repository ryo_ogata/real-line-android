package pw.oganity.realline.fragment;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import pw.oganity.realline.R;
import pw.oganity.realline.activity.CountryListActivity;
import pw.oganity.realline.vo.CountryListItemVo;

/**
 * Created by rogata on 2015/05/29.
 */
public class TownListFragment extends ListFragment {

    private final static String TAG = "CountryListFragment";

    private LayoutInflater mInfrater;
    private ArrayAdapter<CountryListItemVo> mAdapter;
    private CountryListItemVo mParentCountry;
    private List<CountryListItemVo> mChildTowns;

    public static TownListFragment newInstance(CountryListItemVo parentModel, List<CountryListItemVo> childModels) {
        TownListFragment fragment = new TownListFragment();
        fragment.mParentCountry = parentModel;
        fragment.mChildTowns = childModels;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mInfrater = getActivity().getLayoutInflater();
        mAdapter = new ArrayAdapter<CountryListItemVo>(this.getActivity(), 0) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (convertView == null || convertView.getTag() == null) {
                    convertView = mInfrater.inflate(R.layout.place_list_item_town_view, null, false);
                    convertView.setTag(new ViewHolder(convertView));
                }
                CountryListItemVo vo = getItem(position);
                ViewHolder h = (ViewHolder) convertView.getTag();
                if (position == 0) {
                    h.mCountryNameTextView.setText("All");
                } else {
                    h.mCountryNameTextView.setText(vo.getCountryName());
                }
                return convertView;
            }

            class ViewHolder {
                TextView mCountryNameTextView;

                ViewHolder(View v) {
                    mCountryNameTextView = (TextView) v.findViewById(R.id.town_name_view);
                }
            }
        };
        setListAdapter(mAdapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mAdapter.add(mParentCountry);
        mAdapter.addAll(mChildTowns);
        mAdapter.notifyDataSetChanged();
        return inflater.inflate(R.layout.fragment_country_list, null, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getActionBar().setTitle(mParentCountry.getCountryName());
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        CountryListItemVo vo = mAdapter.getItem(position);
        Log.i(TAG, vo.toString());
        startTrendsHashtagListFragment(vo);
    }

    private void startTrendsHashtagListFragment(CountryListItemVo vo) {
        ((CountryListActivity) getActivity()).pushFragment(
                TrendsHashtagListFragment.newInstance(vo)
        );
    }

}
