package pw.oganity.realline.fragment;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mopub.nativeads.MoPubAdAdapter;
import com.mopub.nativeads.MoPubNativeAdPositioning;
import com.mopub.nativeads.MoPubNativeAdRenderer;
import com.mopub.nativeads.ViewBinder;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.tweetui.SearchTimeline;
import com.twitter.sdk.android.tweetui.TimelineResult;
import com.twitter.sdk.android.tweetui.TweetTimelineListAdapter;
import com.twitter.sdk.android.tweetui.UserTimeline;

import java.util.List;

import pw.oganity.realline.BuildConfig;
import pw.oganity.realline.R;
import pw.oganity.realline.SessionRecorder;

public class TimeLineFragment extends ListFragment {

    private static final String TAG = "TimeLineFragment";

    MoPubAdAdapter mAdAdapter;

    TextView mNoTweetsTextView;
    ProgressBar mLoadingView;
    private List<String> mHashtags;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    public TweetTimelineListAdapter mTweetTimelineListAdapter;
    private LayoutInflater mInflater;

    public TimeLineFragment() {
    }

    public static TimeLineFragment newInstance() {
        TimeLineFragment fragment = new TimeLineFragment();
        return fragment;
    }

    public static TimeLineFragment newInstance(List<String> hashtags) {
        TimeLineFragment fragment = new TimeLineFragment();
        fragment.mHashtags = hashtags;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.mInflater = inflater;
        ViewGroup view = (ViewGroup) mInflater.inflate(R.layout.fragment_timeline, null, false);
        mLoadingView = (ProgressBar) view.findViewById(R.id.loading);
        mNoTweetsTextView = (TextView) view.findViewById(R.id.no_tweets);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mTweetTimelineListAdapter.refresh(new Callback<TimelineResult<Tweet>>() {
                    @Override
                    public void success(Result<TimelineResult<Tweet>> result) {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }

                    @Override
                    public void failure(TwitterException e) {
                        // nothing to do
                    }
                });
            }
        });
        mSwipeRefreshLayout.setColorSchemeResources(
                R.color.tw__blue_default, R.color.white,
                R.color.tw__blue_default, R.color.white);

        final TwitterSession activeSession = SessionRecorder.getTwitterSession();
        if (mHashtags == null) {
            UserTimeline timeline = new UserTimeline.Builder()
                    .userId(activeSession.getUserId())
                    .includeReplies(true)
                    .includeRetweets(false)
                    .maxItemsPerRequest(50)
                    .build();
            mTweetTimelineListAdapter = new TweetTimelineListAdapter(getActivity(), timeline);
        } else {
            SearchTimeline timeline = new SearchTimeline.Builder()
                    //.languageCode(Locale.getDefault().getLanguage().toLowerCase())
                    .query(createSearchQuery())
                    .maxItemsPerRequest(50)
                    .build();
            mTweetTimelineListAdapter = new TweetTimelineListAdapter(getActivity(), timeline);
        }

        MoPubNativeAdPositioning.MoPubServerPositioning adPositioning = MoPubNativeAdPositioning.serverPositioning();
        mAdAdapter = new MoPubAdAdapter(getActivity(), mTweetTimelineListAdapter, adPositioning);
        final MoPubNativeAdRenderer adRenderer = new MoPubNativeAdRenderer(
                new ViewBinder.Builder(R.layout.native_ad_list_item)
                        .titleId(R.id.native_title)
                        .textId(R.id.native_text)
                        .mainImageId(R.id.native_main_image)
                        .iconImageId(R.id.native_icon_image)
                        .callToActionId(R.id.native_cta)
                        .build());
        mAdAdapter.registerAdRenderer(adRenderer);
        this.setListAdapter(mAdAdapter);

        return view;
    }

    private String createSearchQuery() {
        StringBuffer sb = new StringBuffer();
        String or = " OR ";
        for (String hashtag : mHashtags) {
            sb.append(hashtag)
                    .append(or);
        }
        return sb.substring(0, sb.length() - or.length());
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        getListView().addHeaderView(mSearchView);
        getListView().setEmptyView(mLoadingView);
    }

    @Override
    public void onResume() {
        if (SessionRecorder.getTwitterSession() != null) {
            mAdAdapter.loadAds(BuildConfig.MOPUB_UNIT_ID);
            super.onResume();
        } else {
            super.onResume();
            Toast.makeText(getActivity(),
                    getResources().getString(R.string.toast_twitter_session_invalid),
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mAdAdapter.destroy();
    }

}
