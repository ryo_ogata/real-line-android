package pw.oganity.realline.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.gms.analytics.HitBuilders;
import com.mopub.nativeads.MoPubAdAdapter;
import com.mopub.nativeads.MoPubNativeAdPositioning;
import com.mopub.nativeads.MoPubNativeAdRenderer;
import com.mopub.nativeads.ViewBinder;
import com.nineoldandroids.animation.Animator;
import com.twitter.sdk.android.tweetui.SearchTimeline;
import com.twitter.sdk.android.tweetui.TweetTimelineListAdapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmResults;
import pw.oganity.realline.App;
import pw.oganity.realline.R;
import pw.oganity.realline.activity.CountryListActivity;
import pw.oganity.realline.realm.Bucket;
import pw.oganity.realline.realm.Hashtag;

public class SearchWordTimeLineFragment extends ListFragment {

    private static final String TAG = "TimeLineFragment";

    MoPubAdAdapter mAdAdapter;

    TextView mNoTweetsTextView;
    ProgressBar mLoadingView;
    String word;
    private Button mAddBucketButton;

    public SearchWordTimeLineFragment() {
    }

    public static SearchWordTimeLineFragment newInstance(String word) {
        SearchWordTimeLineFragment fragment = new SearchWordTimeLineFragment();
        fragment.word = word;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.fragment_search_word_timeline, null, false);
        mLoadingView = (ProgressBar) view.findViewById(R.id.loading);
        mNoTweetsTextView = (TextView) view.findViewById(R.id.no_tweets);

        SearchTimeline timeline = new SearchTimeline.Builder()
                //.languageCode(Locale.getDefault().getLanguage().toLowerCase())
                .maxItemsPerRequest(30)
                .query(word)
                .build();
        TweetTimelineListAdapter adapter = new TweetTimelineListAdapter(getActivity(), timeline);

        MoPubNativeAdPositioning.MoPubServerPositioning adPositioning = MoPubNativeAdPositioning.serverPositioning();
        mAdAdapter = new MoPubAdAdapter(getActivity(), adapter, adPositioning);
        final MoPubNativeAdRenderer adRenderer = new MoPubNativeAdRenderer(
                new ViewBinder.Builder(R.layout.native_ad_list_item)
                        .titleId(R.id.native_title)
                        .textId(R.id.native_text)
                        .mainImageId(R.id.native_main_image)
                        .iconImageId(R.id.native_icon_image)
                        .callToActionId(R.id.native_cta)
                        .build());
        mAdAdapter.registerAdRenderer(adRenderer);
        this.setListAdapter(mAdAdapter);

        mAddBucketButton = (Button) view.findViewById(R.id.add_bucket_button);
        mAddBucketButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showChooseBucketDialog();
            }
        });

        return view;
    }

    int itemsIndex = 0;

    private void showChooseBucketDialog() {
        Realm realm = Realm.getInstance(getActivity());
        final RealmResults<Bucket> buckets = realm.where(Bucket.class).findAllSorted("createdAt");
        List<String> items = new ArrayList<String>();
        for (Bucket bucket : buckets) {
            items.add(bucket.getName());
        }
        items.add(getString(R.string.add_new_bucket));
        new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.choose_bucket_dialog_title))
                .setSingleChoiceItems(items.toArray(new String[]{}), itemsIndex, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        itemsIndex = which;
                    }
                })
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        if (itemsIndex < buckets.size()) {
                            Bucket bucket = buckets.get(itemsIndex);
                            boolean added = addHashtag(bucket, word);
                            if (added) {
                                Toast.makeText(getActivity(),
                                        getString(R.string.complete_add_tag),
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(),
                                        getString(R.string.already_exists_tag),
                                        Toast.LENGTH_SHORT).show();
                            }
                            ((CountryListActivity) getActivity()).startHomeActivity();
                        } else {
                            // New Bucket
                            showNewBucketNameDialog(word);
                        }
                    }
                })
                .setNegativeButton(android.R.string.cancel, null)
                .show();
    }

    private void showNewBucketNameDialog(final String hashtag) {
        final EditText editText = new EditText(getActivity());
        editText.setSingleLine(true);
        editText.setText(hashtag);
        new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.enter_bucket_name_dialog_title))
                .setView(editText)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        registerNewBucket(editText.getText().toString(), hashtag);
                        Toast.makeText(getActivity(),
                                getString(R.string.complete_create_bucket),
                                Toast.LENGTH_SHORT).show();
                        ((CountryListActivity) getActivity()).startHomeActivity();
                    }
                })
                .setNegativeButton(android.R.string.cancel, null)
                .show();
    }

    private void registerNewBucket(String bucketName, String word) {
        Realm realm = Realm.getInstance(getActivity());
        realm.beginTransaction();
        Hashtag hashtag = realm.where(Hashtag.class).equalTo("name", word).findFirst();
        if (hashtag == null) {
            hashtag = realm.createObject(Hashtag.class);
            hashtag.setUuid(UUID.randomUUID().toString());
            hashtag.setName(word);
            hashtag.setCreatedAt(new Date());
        }
        Bucket newBucket = realm.createObject(Bucket.class);
        newBucket.setUuid(UUID.randomUUID().toString());
        newBucket.setName(bucketName);
        newBucket.getHashtags().add(hashtag);
        newBucket.setCreatedAt(new Date());
        realm.commitTransaction();
        realm.close();
        App.getInstance().sendCreateBucket(newBucket.getName());
    }

    private boolean addHashtag(Bucket bucket, String word) {
        Realm realm = Realm.getInstance(getActivity());
        realm.beginTransaction();
        Hashtag hashtag = realm.where(Hashtag.class).equalTo("name", word).findFirst();
        if (hashtag == null) {
            hashtag = realm.createObject(Hashtag.class);
            hashtag.setUuid(UUID.randomUUID().toString());
            hashtag.setName(word);
        }
        if (bucket.getHashtags().contains(hashtag)) {
            return false;
        } else {
            bucket.getHashtags().add(hashtag);
        }
        realm.commitTransaction();
        realm.close();
        App.getInstance().sendAddHashtag(word);
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getActionBar().setTitle(word);
        bounceButton(0);
    }

    private void bounceButton(int delay) {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                YoYo.with(Techniques.Bounce)
                        .duration(1200)
                        .withListener(new Animator.AnimatorListener() {

                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                bounceButton(1200);
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        })
                        .playOn(mAddBucketButton);
            }
        }, delay);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getListView().setEmptyView(mLoadingView);
        View view = getView();
        mLoadingView = (ProgressBar) view.findViewById(R.id.loading);
        mNoTweetsTextView = (TextView) view.findViewById(R.id.no_tweets);

        SearchTimeline timeline = new SearchTimeline.Builder()
                //.languageCode(Locale.getDefault().getLanguage().toLowerCase())
                .maxItemsPerRequest(30)
                .query(word)
                .build();
        TweetTimelineListAdapter adapter = new TweetTimelineListAdapter(getActivity(), timeline);

        MoPubNativeAdPositioning.MoPubServerPositioning adPositioning = MoPubNativeAdPositioning.serverPositioning();
        mAdAdapter = new MoPubAdAdapter(getActivity(), adapter, adPositioning);
        final MoPubNativeAdRenderer adRenderer = new MoPubNativeAdRenderer(
                new ViewBinder.Builder(R.layout.native_ad_list_item)
                        .titleId(R.id.native_title)
                        .textId(R.id.native_text)
                        .mainImageId(R.id.native_main_image)
                        .iconImageId(R.id.native_icon_image)
                        .callToActionId(R.id.native_cta)
                        .build());
        mAdAdapter.registerAdRenderer(adRenderer);
        this.setListAdapter(mAdAdapter);

        Button addBucketButton = (Button) view.findViewById(R.id.add_bucket_button);
        addBucketButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showChooseBucketDialog();
            }
        });
    }

}
