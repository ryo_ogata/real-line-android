package pw.oganity.realline.fragment;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import pw.oganity.realline.R;
import pw.oganity.realline.SessionRecorder;
import pw.oganity.realline.activity.CountryListActivity;
import pw.oganity.realline.adapter.CountryListAdapter;
import pw.oganity.realline.api.RLTwitterApiClient;
import pw.oganity.realline.api.model.TrendsAvailableModel;
import pw.oganity.realline.vo.CountryListItemVo;

/**
 * Created by rogata on 2015/05/29.
 */
public class CountryListFragment extends ListFragment {

    private final static String TAG = "CountryListFragment";

    private ArrayAdapter<CountryListItemVo> mAdapter;
    private List<TrendsAvailableModel> mModels;

    public static CountryListFragment newInstance() {
        return new CountryListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new CountryListAdapter(this.getActivity(), R.layout.place_list_item_view);
        setListAdapter(mAdapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        new RLTwitterApiClient(SessionRecorder.getTwitterSession())
                .getTrendsService().trendsAvailable(new Callback<List<TrendsAvailableModel>>() {
            @Override
            public void success(Result<List<TrendsAvailableModel>> result) {
                mModels = result.data;
                List<CountryListItemVo> countryListItemVos = new ArrayList<>();
                for (TrendsAvailableModel model : mModels) {
                    // 第1階層のみで絞り込み
                    if (model.getParentId() != 1 && model.getParentId() != 0) {
                        continue;
                    }
                    countryListItemVos.add(CountryListItemVo.newInstance(model));
                }
                Collections.sort(countryListItemVos, new Comparator<CountryListItemVo>() {
                    @Override
                    public int compare(CountryListItemVo o1, CountryListItemVo o2) {
                        int diff = o2.getIsWorldWide().compareTo(o1.getIsWorldWide());
                        if (diff != 0) {
                            return diff;
                        }
                        diff = o2.getIsMyCountry().compareTo(o1.getIsMyCountry());
                        if (diff != 0) {
                            return diff;
                        }
                        return o1.getCountryName().compareTo(o2.getCountryName());
                    }
                });
                mAdapter.clear();
                mAdapter.addAll(countryListItemVos);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void failure(TwitterException e) {
                throw e;
            }
        });
        return inflater.inflate(R.layout.fragment_country_list, null, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getActionBar().setTitle("Choose Country");
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        CountryListItemVo parent = mAdapter.getItem(position);
        List<CountryListItemVo> childs = new ArrayList<>();
        for (TrendsAvailableModel model : mModels) {
            if (model.getParentId() != 1 &&
                    model.getParentId() == parent.getWoeid()) {
                childs.add(CountryListItemVo.newInstance(model));
            }
        }
        if (childs.isEmpty()) {
            startTrendsHashtagListFragment(parent);
        } else {
            startTownListFragment(parent, childs);
        }
    }

    private void startTownListFragment(CountryListItemVo parent, List<CountryListItemVo> childs) {
        ((CountryListActivity) getActivity()).pushFragment(
                TownListFragment.newInstance(parent, childs)
        );
    }

    private void startTrendsHashtagListFragment(CountryListItemVo vo) {
        ((CountryListActivity) getActivity()).pushFragment(
                TrendsHashtagListFragment.newInstance(vo)
        );
    }
}
