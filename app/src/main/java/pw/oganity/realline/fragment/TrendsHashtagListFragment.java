package pw.oganity.realline.fragment;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Session;
import com.twitter.sdk.android.core.TwitterException;

import java.util.List;

import pw.oganity.realline.R;
import pw.oganity.realline.SessionRecorder;
import pw.oganity.realline.activity.CountryListActivity;
import pw.oganity.realline.api.RLTwitterApiClient;
import pw.oganity.realline.api.model.TrendsModel;
import pw.oganity.realline.api.model.TrendsPlaceModel;
import pw.oganity.realline.vo.CountryListItemVo;

/**
 * Created by rogata on 2015/05/29.
 */
public class TrendsHashtagListFragment extends ListFragment {

    private final static String TAG = "TrendsHashtagListFragment";
    private LayoutInflater mInfrater;

    private ArrayAdapter<TrendsModel> mAdapter;
    private TrendsPlaceModel mModel;
    private CountryListItemVo mCountryListVo;

    public static TrendsHashtagListFragment newInstance(CountryListItemVo vo) {
        TrendsHashtagListFragment fragment = new TrendsHashtagListFragment();
        fragment.mCountryListVo = vo;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mInfrater = getActivity().getLayoutInflater();
        mAdapter = new ArrayAdapter<TrendsModel>(this.getActivity(), R.layout.place_list_item_view) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (convertView == null) {
                    convertView = mInfrater.inflate(R.layout.trends_hashtag_list_item_view, null, false);
                    convertView.setTag(new ViewHolder(convertView));
                }
                TrendsModel model = getItem(position);
                ViewHolder h = (ViewHolder) convertView.getTag();
                h.mHashtagView.setText(model.getHashtag());
                return convertView;
            }

            class ViewHolder {
                TextView mHashtagView;

                public ViewHolder(View convertView) {
                    mHashtagView = (TextView) convertView.findViewById(R.id.hashtag_item);
                }
            }
        };
        setListAdapter(mAdapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        final Session activeSession = SessionRecorder.getTwitterSession();
        new RLTwitterApiClient(activeSession)
                .getTrendsService()
                .trendsPlace(mCountryListVo.getWoeid(), new Callback<List<TrendsPlaceModel>>() {
                    @Override
                    public void success(Result<List<TrendsPlaceModel>> result) {
                        mModel = result.data.get(0);
                        mAdapter.clear();
                        mAdapter.addAll(mModel.getTrends());
                        mAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void failure(TwitterException e) {
                        throw e;
                    }
                });
        return inflater.inflate(R.layout.fragment_trends_hastag_list, null, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getActionBar().setTitle(mCountryListVo.getCountryName());
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        TrendsModel item = mAdapter.getItem(position);
        startSearchWordTimelineFragment(item.getHashtag());
    }

    private void startSearchWordTimelineFragment(String hashtag) {
        ((CountryListActivity) getActivity()).pushFragment(
                SearchWordTimeLineFragment.newInstance(hashtag)
        );
    }

}
