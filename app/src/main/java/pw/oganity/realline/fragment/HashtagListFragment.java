package pw.oganity.realline.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseExpandableListAdapter;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.List;

import io.realm.Realm;
import pw.oganity.realline.App;
import pw.oganity.realline.R;
import pw.oganity.realline.activity.CountryListActivity;
import pw.oganity.realline.realm.Bucket;
import pw.oganity.realline.realm.Hashtag;

/**
 * Created by rogata on 2015/05/29.
 */
public class HashtagListFragment extends Fragment {

    private final static String TAG = "HashtagListFragment";

    private List<Bucket> mBuckets;
    private ExpandableListView mExpandListView;
    private BucketArrayAdapter mAdapter;

    public static HashtagListFragment newInstance() {
        return new HashtagListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toast.makeText(getActivity(), R.string.edit_on_longpress, Toast.LENGTH_SHORT).show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hashtag_list, null, false);
        mExpandListView = (ExpandableListView) view.findViewById(R.id.expand_list_view);
        mAdapter = new BucketArrayAdapter(this.getActivity(), inflater, mBuckets);
        mExpandListView.setAdapter(mAdapter);
        mExpandListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int j, long l) {
                startSearchWordTimelineFragment(
                        mBuckets.get(i).getHashtags().get(j).getName()
                );
                return false;
            }
        });
        mExpandListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int childPosition, long id) {
                int itemType = ExpandableListView.getPackedPositionType(id);

                if (itemType == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
                    childPosition = ExpandableListView.getPackedPositionChild(id);
                    int groupPosition = ExpandableListView.getPackedPositionGroup(id);
                    Bucket bucket = mBuckets.get(groupPosition);
                    Hashtag hashtag = bucket.getHashtags().get(childPosition);
                    showDeleteConfirmDialog(bucket, hashtag);
                    return true;
                } else if (itemType == ExpandableListView.PACKED_POSITION_TYPE_GROUP) {
                    int groupPosition = ExpandableListView.getPackedPositionGroup(id);
                    Bucket bucket = mBuckets.get(groupPosition);
                    showRenameDialog(bucket);
                    return true;
                }

                return false;
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mBuckets =
                Realm.getInstance(getActivity()).where(Bucket.class).findAllSorted("createdAt");
        mAdapter.setBuckets(mBuckets);
        mAdapter.notifyDataSetChanged();
    }

    private void showRenameDialog(final Bucket bucket) {
        final EditText editText = new EditText(getActivity());
        editText.setSingleLine(true);
        editText.setText(bucket.getName());
        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.rename_dialog_title)
                .setView(editText)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (i == AlertDialog.BUTTON_POSITIVE) {
                            renameBucket(editText.getText().toString(), bucket);
                            Toast.makeText(getActivity(),
                                    getString(R.string.complete_rename_bucket),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setNegativeButton(android.R.string.cancel, null)
                .show();
    }

    private void renameBucket(String s, Bucket bucket) {
        Realm realm = Realm.getInstance(getActivity());
        realm.beginTransaction();
        bucket.setName(s);
        realm.commitTransaction();
        App.getInstance().sendRenameBucket(bucket.getName());
        reloadExpandableView();
    }

    private void showDeleteConfirmDialog(final Bucket bucket, final Hashtag hashtag) {
        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.hashtag_delete_confirm_title)
                .setMessage(getString(R.string.hashtag_delete_confirm_message, hashtag.getName()))
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (i == AlertDialog.BUTTON_POSITIVE) {
                            deleteHashtag(bucket, hashtag);
                        }
                    }
                })
                .setNegativeButton(android.R.string.cancel, null)
                .show();
    }

    private void deleteHashtag(Bucket bucket, Hashtag hashtag) {
        Realm realm = Realm.getInstance(getActivity());
        realm.beginTransaction();
        bucket.getHashtags().remove(hashtag);
        if (bucket.getHashtags().size() == 0) {
            bucket.removeFromRealm();
        }
        realm.commitTransaction();
//        if (isNoHashtag) {
//            App.getInstance().sendDeleteBucket(bucket.getName());
//        }
//        App.getInstance().sendDeleteHashtag(hashtag.getName());
        reloadExpandableView();
    }

    private void reloadExpandableView() {
        mBuckets =
                Realm.getInstance(getActivity()).where(Bucket.class).findAllSorted("createdAt");
        mAdapter.setBuckets(mBuckets);
        mAdapter.notifyDataSetChanged();
    }

    private void startSearchWordTimelineFragment(String word) {
        startActivity(new Intent(getActivity(), CountryListActivity.class)
                .putExtra(CountryListActivity.SEARCH_WORD, word));
    }

    public class BucketArrayAdapter extends BaseExpandableListAdapter {
        private final SimpleDateFormat sdf;
        private Context mContext;
        private LayoutInflater mInfrater;
        private List<Bucket> mBuckets;

        public BucketArrayAdapter(Context context, LayoutInflater inflater, List<Bucket> buckets) {
            super();
            this.mContext = context;
            this.mInfrater = inflater;
            this.mBuckets = buckets;
            this.sdf = new SimpleDateFormat();
        }

        public void setBuckets(List<Bucket> buckets) {
            this.mBuckets = buckets;
        }

        @Override
        public int getGroupCount() {
            return (mBuckets == null) ? 0 : mBuckets.size();
        }

        @Override
        public int getChildrenCount(int i) {
            return (mBuckets == null) ? 0 : mBuckets.get(i).getHashtags().size();
        }

        @Override
        public Object getGroup(int i) {
            return mBuckets.get(i);
        }

        @Override
        public Object getChild(int i, int j) {
            return mBuckets.get(i).getHashtags().get(j);
        }

        @Override
        public long getGroupId(int i) {
            return i;
        }

        @Override
        public long getChildId(int i, int j) {
            return j;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getGroupView(final int i, final boolean b, View convertView, ViewGroup viewGroup) {
            if (convertView == null) {
                convertView = this.mInfrater.inflate(R.layout.expandable_list_item_buckets, null, false);
            }
            ((TextView) convertView.findViewById(R.id.bucket_name_text)).setText(mBuckets.get(i).getName());
            return convertView;
        }

        @Override
        public View getChildView(final int i, final int j, final boolean b, View convertView, ViewGroup viewGroup) {
            if (convertView == null) {
                convertView = this.mInfrater.inflate(R.layout.expandable_list_item_hashtag, null, false);
            }
            Hashtag hashtag = mBuckets.get(i).getHashtags().get(j);
            ((TextView) convertView.findViewById(R.id.hashtag_name_text)).setText(hashtag.getName());
            ((TextView) convertView.findViewById(R.id.hashtag_created_at_text)).setText(sdf.format(hashtag.getCreatedAt()));
            return convertView;
        }

        @Override
        public boolean isChildSelectable(int i, int j) {
            return true;
        }

        @Override
        public void onGroupCollapsed(int groupPosition) {
            super.onGroupCollapsed(groupPosition);
//            setListViewHeightBasedOnChildren(mExpandListView);
        }

        @Override
        public void onGroupExpanded(int groupPosition) {
            super.onGroupExpanded(groupPosition);
//            setListViewHeightBasedOnChildren(mExpandListView);
            mExpandListView.setSelection(groupPosition);
        }

    }
}
